# Currency Converter

This repo is functionality complete — PRs and issues welcome!

----------

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/7.x/installation)

Clone the repository

    git clone git@bitbucket.org:dbasharanov/currencyconverter.git

Switch to the repo folder

    cd currencyconverter

Install all the dependencies using composer

    composer install

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

Once the laravel/ui package has been installed, you may install the frontend scaffolding using the ui Artisan command:
    
    php artisan ui vue

Install your project's frontend dependencies
    npm install
  
Compiling asset files
    npm run dev
  
**TL;DR command list**

    git clone git@bitbucket.org:dbasharanov/currencyconverter.git
    cd currencyconverter
    composer install
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan serve
    npm run dev