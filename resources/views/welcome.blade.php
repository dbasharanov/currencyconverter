@extends('layouts.app')

@section('content')
    <div class='wall main-wall box-aline-center'>
        <div class='sm-12 text-center'>
            <h1 class='font-54'>Currency Converter</h1>
        </div>
    </div>
    <div class='sm-12'>
        <div class='row'>
            <converter-component></converter-component>
            <banner-component></banner-component>
        </div>
    </div>
@endsection 