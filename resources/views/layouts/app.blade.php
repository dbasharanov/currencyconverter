<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials.head')
</head>
<body>
<div id="app">
    @include('partials.header')

    <main class='sm-12'>
        @yield('content')
    </main>

    @include('partials.footer')
</div>

<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
